function [ gK ] = gasConduction(roh,dF,colDia,molMas,gConstCo,gkCo,gShCo,vCo,T,P)
    
                        %1. Roh = Density
                        %2. dF = Fiber Diameter
                        %3. colDia = Collision Diameter
                        %4. molMas = Molecular Mass
                        %5. gConstCo = Constant Coeficients
                        %6. gkCo = Atmospheric Gass Conductivity Coeficients
                        %7. gShCo = Specific Heat Coeficiants
                        %8. vCo = Viscosity Coeficiants
                        %9. T = Temperature
                        %10.P = Presure 
                                                

    gk = 0;             %Gas Conductivity at atmospheric pressure
    gSh = 0;            %Specific Heat
    v = 0;              %Viscosity
    Kb = 1.3806e-23;    %Boltzman Constant
    alpha = 1;          %Alpha

    for i=1:length(gkCo)
        gk = gk + gkCo(i)*(T^(i-1));
    end
    
    for i=1:length(gShCo)
        gSh = gSh + gShCo(i)*(T^(i-1));
    end
    
    for i=1:length(vCo)-1
        v = v + vCo(i)*(T^(i-1));
    end
    
    v= v * vCo(length(vCo));
    
    gConst = gConstCo / molMas;
    Pr = v * gSh / gk;
    gama = 1/((1-gConst)/gSh);
    lamda = (Kb * T) / (sqrt(2) * pi * P * (colDia^2));
    porS = (pi/4) * (dF/(roh/3300));
    Kn = lamda/porS;
    beta = ((2-alpha)/(alpha)) * (2*gama/(gama + 1));
    phi = 1;
    psi = 1;
    zeta = beta/Pr;
    gK = gk/(phi + 2*psi*zeta*Kn);
   
end