function [ Tnew ] = thomasAlgorithm(a,b,c,d)
    m = length(d);

    cnew(1) = c(1)/b(1);
    for i=2:m-1
        cnew(i) = c(i)/(b(i) - cnew(i-1)*a(i));
    end
    dnew(1) = d(1)/b(1);
    
    for i=2:m
        dnew(i) = (d(i) - dnew(i-1)*a(i))/(b(i)-cnew(i-1)*a(i));
    end
    
    Tnew(m) = dnew(m);
    for i=m-1:-1:1
        Tnew(i) = dnew(i) - cnew(i)*Tnew(i+1);
    end 
end