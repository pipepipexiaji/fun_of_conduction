function [ k ] = solidConduction(kCo, T)
    k = 0;
    for i=1:length(kCo)
        k = kCo(i) * (T^(i-1)) + k;
    end
end