function [] = transfer(mat,gas,enviro)
%  This function uses material, gas and envrionmental properties to
%  calculate 1D heat distribution throughout a material.

%  Default Properties: Saffil Paper and Air at sea level with an ambient 
%  temperature of 300 Degrees Kelvin

layers(1) = mat;                

boltz = 5.67e-8;                    %Boltzman Constant

m = mat.m;                          %Number of Nodes
L = mat.L;                          %Length centimeters
roh = mat.roh;                      %Density
dF = mat.fDia;                      %Fiber Diameter (m)
roh = mat.roh;                      %Density
dx = L/(m-1);                       %Change in X

time = enviro.time;                 %Starting Time                    
finalTime = enviro.finalTime;       %End Time
dt = enviro.dt;                     %Change in Time (s)
P = enviro.P;                       %Presure (Pascal)
step = enviro.step;                 %Step Count
timeHeated = enviro.timeHeated;
eps = enviro.eps;                   %Epsilon
ambient = enviro.ambient;           %Ambient Temperature
q = enviro.q;                       %Heat Flux wats/cm^2

for i=1:m                           %Node Initilization
    x(i) = (i-1)*dx;
    layers(1).temp(i) = ambient;
    temp(i) = ambient;
end

Time(1) = 1;                        %Start Time


while time <= finalTime             %Begin Loop
    
    if time < timeHeated
        Q = q * 10000;              %Heating Parameters
    else
        Q = 0;
    end
    
    for i=1:m                       %Calculate Conducivitys
        k(i) = gasConduction(mat.roh, mat.fDia, gas.colDia, gas.molMas, gas.constCo, gas.kCo, gas.CpCo, gas.vCo, temp(i), P) + solidK(mat.kCo,temp(i));
        h(i) = specificHeat(1276,3.2e-3,temp(i));
    end
    
    for i=2:m                       %Initialize TriDiagnal Matrix
        a(i) = -(k(i-1)/(2*dx));
        b(i) = (dx * roh * h(i) / dt) + (k(i-1)/(2*dx)) + (k(i)/(2*dx));
        c(i) = -(k(i)/(2*dx));
        if i ~= m 
            d(i) = (k(i-1)/(2*dx))*(temp(i-1) - temp(i)) + (k(i)/(2*dx))*(temp(i+1) - temp(i)) + (dx * roh * h(i) / dt)*temp(i);
        end
    end
                                     %Matrix Value Exceptions
    b(1) = (dx * roh * h(1) / dt) + (k(1)/(2*dx));
    c(1) = -(k(1)/(2*dx));
    d(1) = (dx * roh * h(1) / dt)*temp(1) + (k(i)/(2*dx))*(temp(2) - temp(1)) + Q - (eps * boltz * (temp(1)^4 - ambient^4));
    
    b(m) = (dx * roh * h(i) / dt) + (k(m-1)/(2*dx));
    d(m) = (dx * roh * h(m) / dt)*temp(m) + (k(m-1)/(2*dx))*(temp(m-1)-temp(m)); 
    
    

    temp = thomasAlgorithm(a,b,c,d);  %Call Thomas Algorithm
    layers(1).temp = temp;
    
    Time(step) = time;                %Set time of current step
    Temperature(:,step) = temp;       %Set Temperatures at current step
    
    time = time + dt;                 %Increment Time
    step = step + 1;                  %Increment Step
end
    
I = 1;                                %Reverse Final Temperature array
for i=m:-1:1;
    tPrime(i) = temp(I);
    I = I + 1;
end

                                      %Experimental Values
valsX = [.829*0.0254, .522*0.0254, .237*0.0254];
valsY = [0, 0, 0];
    
%plot(x,tPrime,valsX,valsY),xlabel('x'),ylabel('T(x)');

                                      %Output
display(temp(1));
display(temp(m));
display(Temperature(2,5));
plot(Time,Temperature(1,:),Time,Temperature(m/2,:),Time,Temperature(m,:))

end
